import Vue from 'vue'
import App from './App'
import func from './utils/function.js'
const Func = new func();
// 在main.js中引入然后挂载待vue原型
Vue.prototype.$dianji = Func.dianji;
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
