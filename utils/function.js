let request = require("./request.js");
class app_function {
	constructor(arg) {
		
	}
	/**
	 * 判断变量是否有赋值
	 * 原始对象数组 obj
	 * 变量名 variable
	 * @param {Object} obj			原始对象数组
	 * @param {Object} variable		变量名
	 */
	is_var(obj, variable)
	{
		if(obj[variable] == '' || obj[variable] == null || obj[variable] == undefined || obj[variable] == 0){
			return false;
		}else{
			return true;
		}
	}
	
	/** 
	 * 定义数组变量	that.def_arr['def_name'] = def_value
	 * @param {Object} that			变量指向
	 * @param {Object} def_name		定义变量名
	 * @param {Object} def_value	定义变量值
	 */
	def_val(that, def_arr, def_name, def_value)
	{
		if(def_name == null){
			return that[def_arr] = def_value;
		}
		return that[def_arr][def_name] = def_value;
	}
	
	/**
	 * 请求接口 回调Response
	 * @param {Object} url			地址
	 * @param {Object} PostData		传参
	 * @param {Object} Response		响应
	 */
	PostApi(url, PostData, Response)
	{
		new request.Request(url).request(
			()=>{
				return 'POST'
			},
			()=>{
				return PostData;
			},
			(res)=>{
				Response(res);
			}
		);
	}
	
	/**
	 * initArr@ 初始数组 
	 * selectData@ 需要查找的数据 int类型数值行
	 * judge@ 需要合查找的数据 进行判断的名
	 * value@ 表示需要查找的数据在 初始数组的下标位置 给予赋值变量名
	 * postData@ 需要赋予传参的变量
	 */
	GetParamList(initArr, selectData, judge, value, postData, that){
		that[initArr].map((e,n)=>{
			if(e[judge] == selectData){
				that.data[value] = n;
				that[postData] = e[judge];
				return;
			}
		});
	}
	
	/**
	 * 监听多次点击
	 * @param {Object} fn
	 */
	dianji(fn) {
	    let that = this;
	    if (that.onoff) {
	        that.onoff = false;
	        fn();
	        setTimeout(function () {
	            that.onoff = true;
	        }, 3000)
	    } else {
	        //如果一直走else分支可能是你没有在页面的data下面挂载onoff:true,不然一直都会走else
	        new request.Request().showToast('操作频繁,请等待此务执行完毕');
	    }
	}
}


export default app_function;