
// 本地调试 true 
let debug = true;
if(debug){
	var host = "http://localhost";			//	host替换成微信小程序认证的域名
}else{
	var host = "https://api.yihua.pub";
}					
let requestUrl = host + '/api/renthouse/';	// 	租房实际地址

let config = {
  service: {
    host,			// 域名
	requestUrl,		// 请求地址
	tipMoney: '10%',	// 设置服务费显示
  }
};
 
// export {host};
// export {config};


export default config;